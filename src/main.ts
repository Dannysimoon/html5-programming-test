//Author: SungIn Moon
//My focus while programming this game was trying get the fundamemtals right
//rather than adding many features.
//I received a lot of help from: https://phaser.io/examples/v2/games/breakout
//Since it was my first time using phaser and needed help with the API.


//every 100 points player get a random powerup(currently there are 2, bigger paddle or extra life)
//every 5 paddle bounce player receives a random powerdown(currently there are 2, smaller paddle or ball speedup)
//every new round awards player with 100 points and speeds up ball.


/// <reference path="../node_modules/phaser/typescript/phaser.comments.d.ts" />

import {Breakout} from './Breakout';


//matching the game size to the browser size.
//tested on mobile browser emulator, works except the text doesnt scale well.
var game_width = window.innerWidth;
var game_height = window.innerHeight;
if(game_width<(game_height*(800/480))){
	game_height = game_width/(800/480);
} else{
	game_width = game_height*(800/480);
}

	
//Creating new Breakout game
var game = new Breakout(game_width-20, game_height-20, Phaser.AUTO, 'Breakout_example', {preload: preload_main, create: create_main, update: update_main});


function preload_main() {
	game.preload_breakout();
}

function create_main() {
	game.create_breakout();
}

function update_main() {
	game.update_breakout();
}