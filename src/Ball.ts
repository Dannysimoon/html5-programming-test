export class Ball extends Phaser.Sprite {
	//thought about creating helper ball for a powerup and creating another class.
	//but not enough time
    constructor(game, start_x, start_y, ball_texture) {
        super(game, start_x, start_y, ball_texture);
		game.add.existing(this);
		
		this.anchor.setTo(0.5, 0.5);
		this.checkWorldBounds = true;
		
		game.physics.enable(this, Phaser.Physics.ARCADE);

		this.body.collideWorldBounds = true;
		this.body.bounce.set(1);
		
    }
}