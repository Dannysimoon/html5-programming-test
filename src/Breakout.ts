import {Ball} from './Ball';
import {Paddle} from './Paddle';

export class Breakout extends Phaser.Game {
	
	//initialising all variables
	private x_pixels;
	private y_pixels;
	
	private ball_texture;
	private paddle_texture;
	private brick_texture;
	
	private bricks;
	
	private paddle;
	private ball;
	
	private ballOnPaddle;
	private isGameOver;

	private lives;
	private score;
	
	private scoreText;
	private livesText;
	private hitText
	private introText;
	
	private ball_diameter;
	private ball_start_x
	private ball_start_y;
	
	private paddle_x;
	private paddle_y;
	private paddle_start_x;
	private paddle_start_y;
	
	private brick_x;
	private brick_y;
	private brick_personal_space_x;
	private brick_personal_space_y;
	private bricks_start_position_x;
	private bricks_start_position_y;
	
	private scoreText_x;
	private scoreText_y;
	private livesText_x;
	private livesText_y;
	private hitText_x;
	private hitText_y;
	private introText_x;
	private introText_y;
	
	private current_velocity;
	private minimum_velocity;
	private velocity_increment;
	
	private original_powerup;
	private powerup_milestone;
	private powerup_increments;
	private original_powerdown;
	private powerdown_milestone;
	private powerdown_increments;
	private difficulty;
	private minimum_paddle_width;
	
	private paddle_hit_count;
	
	private powerup_paddle_width;
	
	
    constructor(_x_pixels , _y_pixels , Breakout_renderer , Breakout_parent, Breakout_state) {
        super(_x_pixels , _y_pixels , Breakout_renderer , Breakout_parent , Breakout_state);
		this.x_pixels = _x_pixels;
		this.y_pixels = _y_pixels;
		
		//assigning values to variables
		this.ballOnPaddle = true;
		this.isGameOver = false;
		this.lives = 3;
		this.score = 0;
		
		//paddle size
		this.paddle_x = this.set_resolution(72);
		this.paddle_y = this.set_resolution(18);
		
		this.paddle_start_x = this.x_pixels/2
		this.paddle_start_y = this.y_pixels - this.set_resolution(100);
		
		//ball size
		this.ball_diameter = this.set_resolution(24);
		this.ball_start_x = this.x_pixels/2;
		this.ball_start_y = this.paddle_start_y - this.set_resolution(24);
		
		
		this.brick_x = this.set_resolution(56);
		this.brick_y = this.set_resolution(18);
		this.bricks_start_position_x = this.set_resolution(80);
		this.bricks_start_position_y = this.set_resolution(56);
		this.brick_personal_space_x = this.set_resolution(64);
		this.brick_personal_space_y = this.set_resolution(48);
		
		
		this.scoreText_x = 32;
		this.scoreText_y = this.y_pixels - this.set_resolution(50);
		this.livesText_x = this.x_pixels - 120;
		this.livesText_y = this.y_pixels - this.set_resolution(50);
		this.hitText_x = this.x_pixels - 180;
		this.hitText_y = this.y_pixels - this.set_resolution(75);
		this.introText_x = this.x_pixels/2
		this.introText_y = this.y_pixels - this.set_resolution(200);
		
		//ball's velocity
		this.current_velocity = this.set_resolution(300);
		this.minimum_velocity = this.set_resolution(300);
		this.velocity_increment = this.set_resolution(100);
		
		//increase these numbers for harder gameplay(or the other way around)
		this.original_powerup = 100;
		this.powerup_milestone = 100;
		this.powerup_increments = 100;
		//increase these numbers for easier gameplay(or the other way around)
		this.original_powerdown = 5;
		this.powerdown_milestone = 5;
		this.powerdown_increments = 5;
		
		this.powerup_paddle_width = this.set_resolution(36);
		this.minimum_paddle_width = this.set_resolution(36);
		
		//higher number = easier
		this.difficulty = 1;
		
		this.paddle_hit_count = 0;
		
    }
	
	
	
	public preload_breakout() {
		//drawing shapes to use as sprites.
		
		var drawer;
		
		drawer = new Phaser.Graphics(this);
		drawer.beginFill(0x00ff00);
		drawer.drawRect(0, 0, this.paddle_x, this.paddle_y);
		this.paddle_texture = drawer.generateTexture();
		drawer.endFill();
		
		drawer = new Phaser.Graphics(this);
		drawer.beginFill(0xff0000);
		drawer.drawCircle(0, 0, this.ball_diameter);
		this.ball_texture = drawer.generateTexture();
		drawer.endFill();
		
		drawer = new Phaser.Graphics(this);
		drawer.beginFill(0x0000ff);
		drawer.drawRect(0, 0, this.brick_x, this.brick_y);
		this.brick_texture = drawer.generateTexture();
		drawer.endFill();
		
	}


	public create_breakout() {
		//initialising
		this.physics.startSystem(Phaser.Physics.ARCADE);
		
		this.physics.arcade.checkCollision.down = false;
		
		//creating brick sprites
		this.bricks = this.add.group();
		this.bricks.enableBody = true;
		this.bricks.physicsBodyType = Phaser.Physics.ARCADE;
		
		var brick;
		
		for (var y = 0; y < 5; y++)
		{
			for (var x = 0; x < 10; x++)
			{
				brick = this.bricks.create(this.bricks_start_position_x + (x * this.brick_personal_space_x), 
										   this.bricks_start_position_y + (y * this.brick_personal_space_y), 
										   this.brick_texture);
				brick.body.bounce.set(1);
				brick.body.immovable = true;
			}
		}
		
		//adding paddle and ball to the game
		this.paddle = new Paddle(this,this.paddle_start_x, this.paddle_start_y, this.paddle_texture);
		
		this.ball = new Ball(this, this.ball_start_x , this.ball_start_y, this.ball_texture);
		
		
		this.ball.events.onOutOfBounds.add(this.ballLost, this);
		
		//on screen texts
		this.scoreText = this.add.text(this.scoreText_x, this.scoreText_y, 'score: 0', { font: "20px Arial", fill: "#ffffff", align: "left" });
		this.livesText = this.add.text(this.livesText_x, this.livesText_y, 'lives: 3', { font: "20px Arial", fill: "#ffffff", align: "left" });
		this.hitText = this.add.text(this.hitText_x, this.hitText_y, 'Paddle hits: 0', { font: "20px Arial", fill: "#ffffff", align: "left" });
		this.introText = this.add.text(this.introText_x, this.introText_y, '- click to start -', { font: "40px Arial", fill: "#ffffff", align: "center" });
		this.introText.anchor.setTo(0.5, 0.5);

		//managin input
		this.input.onDown.add(this.input_handler, this);
		
		
	}

	
	public update_breakout(){
		
		//moving paddle
		this.paddle.x = this.input.x;
		
		//stopping paddle on the sides of the game
		if (this.paddle.x < this.set_resolution(24)){
			this.paddle.x = this.set_resolution(24);
		}
		else if (this.paddle.x > this.width - this.set_resolution(24)){
				 this.paddle.x = this.width - this.set_resolution(24);
		}
		
		if (this.ballOnPaddle){
			this.ball.x = this.paddle.x;
		}
		else{
			//checking collision
			this.physics.arcade.collide(this.ball, this.paddle, this.ballHitPaddle, null, this);
			this.physics.arcade.collide(this.ball, this.bricks, this.ballHitBrick, null, this);
		}
		
		//powerups
		if (this.score >= this.powerup_milestone){
			this.powerup_milestone += this.powerup_increments;
			this.powerup();
		}
		
		if (this.paddle_hit_count >= this.powerdown_milestone){
			this.powerdown_milestone += this.powerdown_increments;
			this.powerdown();
		}
	}
	
	private input_handler () {
		
		if (this.ballOnPaddle)
		{
			//calculating initial speed of ball when leaving the paddle
			this.ballOnPaddle = false;
			var random_start_y = Math.random();
			//giving some randomness
			var random_start_x = 1 - random_start_y;
			var temp_start_x = random_start_x;
			var random_direction = 1;
			if (random_start_y < 0.5){
				random_start_x = random_start_y;
				random_start_y = temp_start_x;
				random_direction = -1;
			}
			this.ball.body.velocity.y = Math.sqrt((random_start_y/1) * (this.current_velocity ** 2)) * -1;
			this.ball.body.velocity.x = Math.sqrt((random_start_x/1) * (this.current_velocity ** 2)) * random_direction;
			this.introText.visible = false;
		}
		
		else if(this.isGameOver){
			//resetting game for a new game.
			this.lives = 3;
			this.isGameOver = true;
			this.score = 0;
			this.powerup_milestone = this.original_powerup;
			this.powerdown_milestone = this.original_powerdown;
			this.paddle.width = this.paddle_x;
			this.bricks.callAll('revive',null);
			this.introText.text = '- click to start -';
			this.livesText.text = 'lives: ' + this.lives;
			this.scoreText.text = 'score: ' + this.score;
			this.ballOnPaddle = true;
			this.ball.reset(this.ball_start_x, this.ball_start_y);
			this.current_velocity = this.set_resolution(300);

			
		}

	}
	
	
	private ballLost () {
		//when the ball falls through
		this.lives--;
		this.livesText.text = 'lives: ' + this.lives;
		
		if (this.lives === 0){
			this.gameOver();
		}
		else{
			this.ballOnPaddle = true;
			this.ball.reset(this.ball_start_x, this.ball_start_y);
			//slowing down after death
			
			this.current_velocity = this.current_velocity - this.velocity_increment;
			if(this.current_velocity < this.minimum_velocity){
				this.current_velocity = this.minimum_velocity;
			}
		}
	}
	
	
	private gameOver () {
		
		this.ball.body.velocity.setTo(0, 0);
		
		this.introText.text = 'Game Over! \n Click to Reset';
		this.introText.visible = true;
		this.isGameOver = true;

	}
	
	private ballHitBrick (_ball, _brick) {
		
		_brick.kill();

		this.score += 10;

		this.scoreText.text = 'score: ' + this.score;

		//checking for any bricks
		if (this.bricks.countLiving() == 0)
		{
			//next level
			this.score += 100;
			this.scoreText.text = 'score: ' + this.score;
			this.introText.text = '- Next Level -';
			this.introText.visible = true;

			//resetting ball on the paddle
			this.ballOnPaddle = true;
			this.ball.body.velocity.setTo(0,0);
			this.ball.x = this.ball_start_x;
			this.ball.y = this.ball_start_y;

			//resetting bricks
			this.bricks.callAll('revive',null);
			
			//increasing speed of ball(for difficulty)
			this.current_velocity = this.current_velocity + this.velocity_increment;
		}

	}
	
	private ballHitPaddle (_ball, _paddle) {
		
		//calculating velocity when ball hits paddle
		var diff = 0;
		var diff_velocity = 0;
		var temp_velocity_x;
		var total_temporary_velocity;
		var total_velocity_ratio;
		var new_velocity_x;
		var new_velocity_y;
		
		//changing magnitude of bounce depending on where the ball hits
		diff = _ball.x - _paddle.x;
		diff_velocity = (diff/(this.paddle.width/2)) * (this.current_velocity);
		temp_velocity_x = diff_velocity + _ball.body.velocity.x;
		
		//calculating resulting velocity and ratio to the current velocity
		total_temporary_velocity = (temp_velocity_x ** 2) + (_ball.body.velocity.y ** 2);
		total_velocity_ratio = (this.current_velocity ** 2)/total_temporary_velocity;
		
		//multiplying the ratio so that final velocity remains the same
		new_velocity_x = Math.sqrt((temp_velocity_x ** 2) * total_velocity_ratio);
		new_velocity_y = Math.sqrt((_ball.body.velocity.y ** 2) * total_velocity_ratio) * -1;
		
		//making sure the ball moves to the correct side
		if(temp_velocity_x < 0){
			new_velocity_x = new_velocity_x * -1;
		}
		//making sure the ball doesnt get trapped with no verticle velocity(or very little)
		if(new_velocity_y > -36){
			new_velocity_y = -36;
		}
		
		_ball.body.velocity.x = new_velocity_x;
		_ball.body.velocity.y = new_velocity_y;
		
		//counting paddle hit count for powerdown
		this.paddle_hit_count += 1;
		this.hitText.text = 'Paddle hits: ' + this.paddle_hit_count;
		
		
	}
	
	private powerup(){
		//randomly selects a powerup
		var max = 2;
		var random_powerup;
		random_powerup = Math.floor(Math.random() * max) + 1 
		
		if(random_powerup == 1){
			this.paddle.width += this.powerup_paddle_width;
		}
		else if(random_powerup == 2){
			this.lives += 1;
			this.livesText.text = 'lives: ' + this.lives;
		}
	}
	
	private powerdown(){
		//randomly selects a powerdown
		var max = 2;
		var random_powerdown;
		random_powerdown = Math.floor(Math.random() * max) + 1 
		this.hitText.text = 'Paddle hits: ' + this.paddle_hit_count;
		
		if(random_powerdown == 1){
			this.paddle.width -= (this.powerup_paddle_width/this.difficulty);
			if(this.paddle.width < this.minimum_paddle_width){
				this.paddle.width = this.minimum_paddle_width;
			}
		}
		else if(random_powerdown == 2){
			this.current_velocity = this.current_velocity + (this.velocity_increment/this.difficulty);
		}
	}
	
	private set_resolution(original_resolution){
		//changing resoltion to better fit current resolution
		var new_resolution;
		new_resolution = this.x_pixels/(800/original_resolution);
		return new_resolution;
	}
	

}

 