export class Paddle extends Phaser.Sprite {
	
    constructor(game, start_x, start_y, paddle_texture) {
        super(game, start_x, start_y, paddle_texture);
		game.add.existing(this);
		
		this.anchor.setTo(0.5, 0.5);
		
		game.physics.enable(this, Phaser.Physics.ARCADE);
		
		this.body.collideWorldBounds = true;
		this.body.bounce.set(1);
		this.body.immovable = true;
    }
}